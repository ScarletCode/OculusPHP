<?php

/**
* Cronjob Organiser for the Oculus PHP Class
* @version 0.3
* @author ScarletCode
* @see https://github.com/ScarletCode/OculusPHP
*/
class CronOculus extends Oculus{

	/**
	 * The caching directory
	 * @var [string]
	 */
	public $caching_dir = __DIR__ . '/oculus_cache';

	/**
	 * The output directory
	 * @var [string]
	 */
	public $output_dir = __DIR__ . '/oculus_output';
	
	/**
	 * list of urls with custom callback as an array
	 * @var [array]
	 */
	protected $cron_urls = [];

	/**
	 * Array of keys for the Output Format
	 * @var [array]
	 */
	protected $output_format = [];

	/**
	 * Sets the needed Data and constructs the parent Oculus Class
	 */
	public function __construct(){
		$this->caching_dir = str_replace('\\', '/', $this->caching_dir);
		$this->output_dir = str_replace('\\', '/', $this->output_dir);
		parent::__construct();
	}

	/**
	 * Sets the global Output Format
	 * @param [array] $group List of keys
	 * @return [void]
	 */
	public function set_output_format($group){
		$this->output_format = $group;
	}

	/**
	 * gets a clean object based on the global Output Format
	 * @return [object] a clean instance of the output format
	 */
	public function get_output_format(){
		$format = new stdClass();
		foreach ($this->output_format as $key) {
			$format->{$key} = null;
		}
		return $format;
	}

	/**
	 * Adds a new URL to the Cron Crawler
	 * @param [string] $url  the Url to be added
	 * @param [string] $callback  function name of the custom crawling callback
	 * @return [void]
	 */
	public function add_url($url, $callback){
		$url = [
			'url' => $url,
			'callback' => $callback
		];
		array_push($this->cron_urls, $url);
	}

	/**
	 * Checks if the given URL is the root URL of the current crawl
	 * @param [string] $url  the URL to be checked
	 * @return [boolean]
	 */
	public function is_root_url($url){
		foreach ($this->cron_urls as $urls) {
			if ($urls['url'] == $url) {
				return true;
			}
		}
		return false;
	}

	/**
	 * Kicks of the overall CronCrawler
	 * @return [boolean]
	 */
	public function start(){

		// Outputformat needs to be set
		if ($this->output_format == null) {
			return false;
		}

		// Start
		foreach ($this->cron_urls as $url) {

			// Skip if cached file exists
			if ($this->check_cache($url['url'])) {
				continue;
			}

			$this->set_page($url['url']);
			$this->crawl();

			// User Callback
			$query = call_user_func($url['callback'], $this, $url['url']);

			// Caching + reset
			$this->cache($query, $url['url']);
			$this->reset_settings();
		}
		$this->save();
		return true;
	}

	/**
	 * Checks, if the current URL has allready been crawled by checking the cache folger
	 * @param [string] $url the currently crawled url
	 * @return [void]
	 */
	protected function check_cache($url){
		return is_file($this->caching_dir . '/' . $this->filename(true, $url));
	}

	/**
	 * Builds the updated filename
	 * @param [boolean] $cache  true if filename for caching , false if filename for save
	 * @return [string] the filename
	 */
	protected function filename($cache, $url){
		$url = str_replace(['https', 'http', '/', ':', '.'], ['', '', '_', '', '_'], $url);
		if ($cache) {
			return 'cache_' . $url . '.json';
		}
		else{
			return '';
		}
	}

	/**
	 * Caches the result of the current crawl to the caching folder
	 * @param [array] $query  the result of the current query
	 * @param [string] $url  The current url, which has been crawled
	 * @return [void]
	 */
	protected function cache($query, $url){
		$content = json_encode($query);

		if (!is_dir($this->caching_dir)) {
			mkdir($this->caching_dir);
		}
		file_put_contents($this->caching_dir . '/' . $this->filename(true, $url), $content);
	}

	/**
	 * Saves the combined results of all caches to the output folder
	 * @return [void]
	 */
	protected function save(){
		$content = [];
		foreach ($this->cron_urls as $url) {
			$cache = file_get_contents($this->caching_dir . '/' . $this->filename(true, $url['url']));
			$cache = json_decode($cache);

			foreach ($cache as $key => $res) {
				$key = stripcslashes($key);
				$content[$key] = $res;
			}
		}
		
		$content = json_encode($content);
		if (!is_dir($this->output_dir)) {
			mkdir($this->output_dir);
		}
		file_put_contents($this->output_dir . '/' . 'result' . date('omd') . '.json', $content);
		$this->delete_folder($this->caching_dir);

	}

	/**
	 * Deletes a folder and all its contents from the server
	 * @param [string] $dir the directory to be deleted
	 * @return [void]
	 */
	protected function delete_folder($dir){
		$files = scandir($dir);
		foreach ($files as $file) {
			if (is_file($dir . '/' . $file)) {
				unlink($dir . '/' . $file);
			}
		}
		rmdir($dir);
	}
}