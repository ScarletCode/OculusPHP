<?php

/**
* Basic PHP Crawler
* @version 0.3
* @author ScarletCode
* @see https://github.com/ScarletCode/OculusPHP
*/
class Oculus{

	public function __construct(){
		$this->reset_settings();
	}

	/**
	 * The URL to be crawled
	 * @var [string]
	 */
	protected $url = '';

	/**
	 * The base URL to be crawled
	 * @var [string]
	 */
	protected $root_url = '';

	/**
	 * List of HTML Tags to be excluded from the Crawler
	 * @var [array]
	 */
	public $excludes = [];

	/**
	 * Multidimensional Array containing the entire
	 * crawled homepage
	 * @var [array]
	 */
	protected $result = [];

	/**
	 * Multidimensional Array containing all data
	 * for a single query
	 * @var [array]
	 */
	protected $query = [];

	/**
	 * Array of crawled URLS
	 * @var [array]
	 */
	public $urls = [];

	/**
	 * Resets all Settings to its Initial Value;
	 * @return [void]
	 */
	public function reset_settings(){
		$this->url = '';
		$this->root_url = '';
		// $this->excludes = [];
		$this->result = [];
		$this->query = [];
	}

	/**
	 * Sets the URL for the global Crawler Class
	 * @param [string] $url the new URL
	 * @return [void]
	 */
	public function set_page($url){
		
		if (preg_match('/https?:\/\/.*?\//', $url)) {
			$this->root_url = str_replace(preg_replace('/https?:\/\/.*?\//', '', $url), '', $url);
		}
		elseif(isset($this->root_url)){
			$url = $this->root_url . trim($url, '/');
		}

		$this->url = $url;
	}

	/**
	 * Sets the global excluded HTML Tags
	 * @param [array] $excludes array of HTML Tags to be excluded
	 * @return [void]
	 */
	public function set_excludes($excludes){
		$this->excludes = $excludes;
	}

	
	/**
	 * Initializes the crawling of the current set URL
	 * with the current settings
	 * @return [array]
	 */
	public function crawl(){
		$dom = new DOMDocument();
		@$dom->loadHTMLFile($this->url, LIBXML_NOWARNING);
		$this->result = $this->get_array($dom->childNodes);
		array_push($this->urls, $this->url);
	}

	/**
	 * Returns the inner HTML of the current node
	 * @param  [obj] $node current node object
	 * @return [string] inner HTML
	 */
	protected function get_content($node){
		return $node->ownerDocument->saveHTML($node);
	}

	/**
	 * Returns array with all nodes with the child nodes
	 * as multidimensional array
	 * - including all the data 
	 * @param  [obj] $node Current Node to be passed
	 * @return [array] Current Node data
	 */
	protected function get_array($node){
		$return = [];
		foreach ($node as $child_node) {
			if (isset($child_node->tagName) && !in_array($child_node->tagName, $this->excludes)) {
				array_push($return,
					$append = [
						'tag' => $child_node->tagName,
						'atts' => $this->get_atts($child_node->attributes),
						'content' => $this->get_content($child_node),
						'childs' => $this->get_array($child_node->childNodes)
					]
				);
			}
		}
		return $return;
	}

	/**
	 * Remove repeating and unnessesary blank spaces 
	 * explode every attribute to an array -> helps to find classes
	 * explode ignores the title style and alt attributes
	 * @param  [string] $name the attribute to be cleaned
	 * @return [array] $attr the cleaned attribute
	 */
	protected function clean_attr($name, $attr){
		$attr = trim(preg_replace('/\s+/', ' ', $attr));
		if ($name == 'title' || $name == 'alt' ||$name == 'style') {
			$attr = [$attr];
		}
		else{
			$attr = explode(' ', $attr);
		}
		return $attr;
	}

	/**
	 * Returns an array representing the current
	 * attribtue-objects data as an array
	 * @param  [obj] $attributes current attribute object
	 * @return [array] array with all atts and values
	 */
	protected function get_atts($attributes){
		$return = [];
		foreach ($attributes as $attribute) {
			$return[$attribute->name] = $this->clean_attr($attribute->name, $attribute->value);
		}
		return $return;
	}

	/**
	 * Query the current result by attribute and value
	 * @param  [string] $type the attribute to be checked
	 * @param  [string] $value the value of this attribute
	 * @param  [array] $nodes the current handled nodes
	 * @return [void]
	 */
	protected function get_by_attr($type, $value, $nodes){
		foreach ($nodes as $node) {

			// matches the get parameter
			if (isset($node['atts'][$type]) && in_array($value, $node['atts'][$type])) {
				array_push($this->query, $node);
			}

			// parse childs
			if (count($node['childs']) > 0) {
				$this->get_by_attr($type, $value, $node['childs']);
			}
		}
	}

	/**
	 * Query the current result by tagname
	 * @param  [string] $tag the tagname to be checked
	 * @param  [array] $nodes the current handled nodes
	 * @return [void]
	 */
	protected function get_by_tag($tag, $nodes){
		foreach ($nodes as $node) {
			// matches the get parameter
			if (isset($node['tag']) && $node['tag'] == $tag) {
				array_push($this->query, $node);
			}

			// parse childs
			if (count($node['childs']) > 0) {
				$this->get_by_tag($tag, $node['childs']);
			}
		}
	}

	/**
	 * Starts a query depending of the given opion
	 * @param  [string] $option the type of query to be executed
	 * @param  [string] $value the value to be searched for
	 * @param  [string] $type the type of value to be searched for
	 * @param  [array] $target nodes, where the query is beeing executed
	 * @param  [array] $query old query to be chained here
	 * @return [array] array with all elements in the current query
	 */
	public function get($option, $value, $type = '', $target = false, $query = []){

		// Search globally if no target query has been selected
		if ($target === false) {
			$target = $this->result;
		}		

		// Resets query or appends old querys to this one
		$this->query = $query;

		if ($option == 'attr') {
			$this->get_by_attr($type, $value, $target);
		}
		elseif ($option == 'tag') {
			$this->get_by_tag($value, $target);
		}
		return $this->query;
	}

	/**
	 * Returns an array of all html content of the current query
	 * @param  [$query] $query [the current query]
	 * @return [array]        [array with HTML content]
	 */
	public function value($query){
		$return = [];
		foreach ($query as $item) {
			if (isset($item['content'])) {
				$content = $item['content'];

				// Clean excludes from value
				foreach ($this->excludes as $exclude) {
					$content = preg_replace('/<' . $exclude . '.*?<\\/' . $exclude . '>/s', '', $content);
				}
				array_push($return, $content);
			}
		}
		return $return;
	}

	/**
	 * Follows the first Link in the current query and starts the crawl again
	 * @param  [array] $query [the current query]
	 * @return [boolean]        [success]
	 */
	public function follow($query){
		$href = '';
		foreach ($query as $item) {
			if (isset($item['atts']['href'])) {
				$href = $item['atts']['href'][0];
				break;
			}
		}
		if ($href == '') {
			return false;
		}
		elseif (strpos($href, $this->url) != 0) {
			$href = $this->url . '/' . $href;
		}
		$this->set_page($href);
		$this->crawl();
		return true;
	}

	/**
	 * follows each link in the current query and calls a
	 * user set function to allow further handling
	 * @param  [array] $query  [the current query]
	 * @param  [string] $action [function name as string to be called for further handling]
	 * @return [void]
	 */
	public function follow_each($query, $action){
		$return = [];
		if (function_exists($action)) {
			foreach ($query as $item) {
				$tmp = [];
				array_push($tmp, $item);
				if ($this->follow($tmp)){
					$done = call_user_func($action, $this, $this->url);
					array_push($this->urls, $this->url);

					// Catch default querys
					if (is_array($done)) {
						if (count($done) > 0) {
							$return = array_merge($return, $done);
						}
					}

					// Catch alternative returns
					elseif($done != null){
						array_push($return, $done);
					}
				}
			}
		}
		return $return;
	}

	/**
	 * Limits the count of the current query to a set length
	 * @param  [array] $query [the current query]
	 * @param  [int] $size  [max item count of the reduced array]
	 * @return [array]        [updated query]
	 */
	public function limit($query, $size){
		$new_query = [];
		for ($i=0; $i < count($query) - 1; $i++) { 
			array_push($new_query, $query[$i]);
			if ($i == ($size - 1)) {
				break;
			}
		}
		return $new_query;
	}
}